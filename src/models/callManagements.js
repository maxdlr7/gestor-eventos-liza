const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * This model stores the context that comes from conversation service, this is an unstrict model because the data that comes from the API can change.
 */
//mongoose.set('debug', true);
const CallManagement = new Schema({
    callID: {
        type: 'String',
        required: true
    },
    call: {
        type: Schema.Types.ObjectId,
        ref: 'calls',
        required: true
    },
    tenant: {
        type: Schema.Types.ObjectId,
        ref: 'tenant',
        required: true
    },
    dateAdded: {
        type: 'Date',
        default: Date.now,
        required: true
    },
    queryType: {
        type: 'String',
        required: true
    },
    resolutionStatus: {
        type: 'String',
        default: "Incompleta",//incompleta, completa
        required: true
    },
    finalResult: {
        type: 'String',
        required: false
    },
    dateExpired: {
        type: 'Date',
        required: false
    },
    intention: {
        type: 'String',
        required: false
    },
    duration: {
        type: 'Number',
        default: 0,
        required: false
    },
    sync: {
        type: 'Boolean',
        required: false
    },
}, { strict: false });
const model = mongoose.model('CallManagement', CallManagement);

module.exports = {
    model,
    CallManagement
}
