const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const Schema = mongoose.Schema;

/**
 * Interaction will be store every single time the final user ask something to the chat.
 */
///mongoose.set('debug', true);
const interactionSchema = new Schema({
  tenant: {
    type: Schema.Types.ObjectId,
    ref: 'tenant',
    required: true
  },
  call: {
    type: Schema.Types.ObjectId,
    ref: 'calls',
    required: true
  },
  callManagement: {
    type: Schema.Types.ObjectId,
    ref: 'callManagements',
    required: true
  },
  callID: {
    type: 'String',
    required: true
  },
  contextID: {
    type: 'String',
    required: true
  },
  input: {
    type: 'String',
    required: true
  },
  output: {
    type: Schema.Types.Mixed,
    required: false
  },
  context: {
    type: Schema.Types.Mixed,
    required: true
  },
  intent: {
    type: 'String',
    required: true
  },
  entity: {
    type: 'String',
    required: true
  },
  confidence: {
    type: 'Number',
    required: true
  },
  channel: {
    type: 'String',
    required: true
  },
  workspace: {
    type: 'String',
    required: false
  },
  cuid: {
    type: 'String',
    required: true
  },
  dateAdded: {
    type: 'Date',
    default: Date.now,
    required: true
  },
  isPreview: {
    type: 'Boolean',
    required: false
  },
  sync: {
    type: 'Boolean',
    required: false
  },
}, { strict: false });
interactionSchema.plugin(mongoosePaginate);

const model = mongoose.model('interaction', interactionSchema);

module.exports = {
  model,
  interactionSchema
}
