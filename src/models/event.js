const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const EventSchema = new Schema({
    campaign: {
        type: Schema.Types.ObjectId, ref: 'Campaign',
        required: true
    },
    contact: {
        type: Schema.Types.ObjectId, ref: 'Contact',
        required: false
    },
    action: {
        type: Schema.Types.ObjectId, ref: 'Action',
        required: true
    },
    parent: {
        type: Schema.Types.ObjectId, ref: 'Event',
        required: false
    },
    createdAt: {
        type: 'Date',
        default: Date.now,
        required: true
    },
    updatedAt: {
        type: 'Date',
        default: Date.now,
        required: false
    },
    createdBy: {
        type: Schema.Types.ObjectId, ref: 'User',
        required: true
    },
    updatedBy: {
        type: Schema.Types.ObjectId, ref: 'User',
        required: true
    },
    params: {
        type: 'Mixed',
        required: false
    },
    conditions: {
        type: Array,
        required: false
    },
    schedule: {
        type: 'Date',
        required: true
    },
    status: {
        type: 'String',
        required: true,//active, paused, cancelled, executed
    }
});

module.exports = mongoose.model('Event', EventSchema);


