const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * This model stores the context that comes from conversation service, this is an unstrict model because the data that comes from the API can change.
 */
//mongoose.set('debug', true);
const calls = new Schema({
    callID: { type: 'String', required: true },
    tenant: {
        type: Schema.Types.ObjectId, ref: 'tenant',
        required: true
    },
    name: {
        type: 'String',
        required: false
    },
    email: {
        type: 'String',
        required: false
    },
    channel: {
        type: 'String',
        required: false
    },
    dateAdded: {
        type: 'Date',
        default: Date.now,
        required: true
    },
    sessionStatus: {
        type: 'String',
        default: "ACTIVE",
        required: true
    },
    interactionCount: {
        type: "Number",
        default: 0
    },
    lastInteraction: {
        type: 'Date',
        default: Date.now,
        required: false
    },
    dateExpired: {
        type: 'Date',
        required: false
    },
    phoneNumber: {
        type: 'String',
        required: true
    },
    duration: {
        type: 'Number',
        default: 0,
        required: false
    },
    apiCallsCount: {
        type: 'Number',
        default: 0,
        required: false
    },
    phoneType: {
        type: 'String',
        required: false
    },
    sync: {
        type: 'Boolean',
        required: false
    },

}, { strict: false });

const model = mongoose.model('calls', calls)

module.exports = {
    model,
    calls
}