exports.validationHandler = (req, res, next) => (errors) => {
    if (errors.isEmpty()) {
        return;
    }
    if (!next) {
        res.status(422).json({ errors: errors.array() });
    }
    else {
        return res.status(422).json({ errors: errors.array() });
    }
}