const path = require('path');
const config = {
    assets: path.join(__dirname, 'assets/'),
    feedMenuElements: false,
    language: 'es',
    emailFrom: 'rodriguez.yeinel@gmail.com',
    runCron: false
};

module.exports = config;
