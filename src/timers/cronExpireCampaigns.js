const CronJob = require('cron').CronJob;
const axios = require('axios');

/**
 * Ejecuta el proceso para consultar las campañas y validar si ya expiraron para cambiarles el status
 */
const initCronJobs = async () => {
    /* init cache cron */
    try {
        var cacheJob = new CronJob({
            cronTime: '0 */50 * * * *',//cada 5 minutos
            onTick: async () => {
                try {
                    const config = {
                        method: "post",
                        url: `${process.env.EVENTS_API}/api/v1/campaigns/expire-campaigns`
                    };

                    axios(config).then((response) => {
                        console.log("Job expirar campañas ejecutado  :", new Date());
                    }).catch((err) => {
                        console.error(err);
                    });

                } catch (error) {
                    console.log('Error in initCronJobs');
                }
            },
            start: false
        });
        cacheJob.start();
        console.log('EXPIRES CAMPAIGNS CRON Job started and set to trigger every 5 minutes');
    } catch (error) {
        console.error(error);

    }

};

module.exports = { initCronJobs }
