const CronJob = require('cron').CronJob;
const axios = require('axios');
const Event = require('../controllers/event')

/**
 * Ejecuta el proceso para llamar a la funcion de consulta y ejecucion de eventos
 */
const initCronJobs = async () => {
    /* init cache cron */
    try {
        var cacheJob = new CronJob({
            cronTime: '0 */1 * * * *',//cada 5 minutos
            onTick: async () => {
                try {
                    console.log('Ejecutando cronRunEvents');
                    Event.SyncCollection("calls");
                    Event.SyncCollection("callmanagements");
                    Event.SyncCollection("interactions");
                } catch (error) {
                    console.log('Error in initCronJobs: ', error);
                }
            },
            start: false
        });
        cacheJob.start();
        console.log('CACHE CRON Job started and set to trigger every 5 minutes');
    } catch (error) {
        console.error(error);

    }

};

module.exports = { initCronJobs }
