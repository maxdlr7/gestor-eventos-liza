const cors = require('cors');
const express = require('express');
const morgan = require('morgan');
const helmet = require('helmet');
const cookie = require('cookie-parser');
const body = require('body-parser');
const main = require('../routes/main');
const { keycloak, sessionConfig } = require('../services/keycloak/keycloak');
const swaggerUi = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerOptions = require('./swagger-options.json');

const swaggerSpec = swaggerJSDoc(swaggerOptions);
const expressValidator = require('express-validator');
const Cron = require('../timers/cronRunEvents');
const CronExpireCampaigns = require('../timers/cronExpireCampaigns');
const Event = require('../models/event')
/*
* HTTP server class.
*/
exports.HTTP = class HTTP {


  /*
  * Class constructor.
  */
  constructor({ config } = {}) {
    this.config = config;
    this.app = null;
    this.server = null;
  }


  /*
  * Returns a promise which starts the server.
  */
  async listen() {

    if (this.server) return this;

    // Express configuration
    this.app = express();
    this.app.use(sessionConfig());
    this.app.use(morgan('dev'))
    this.app.use(cookie())
    this.app.use(body.json())
    this.app.use(function (error, req, res, next) {
      if (error.status == 400)
        res.status(400).json({ error: error.message })
      else
        next()
    })
    this.app.use(body.urlencoded({ extended: true }))
    this.app.use(cors())
    this.app.use(helmet())
    this.app.disable('x-powered-by')
    this.app.use(keycloak.middleware());
    this.app.use(expressValidator());

    //Route configuration
    this.app.use('/', main)
    this.app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

    // Event.watch().
    //   on('change', data => console.log(new Date(), data));


    await new Promise((resolve) => {
      let { httpPort, httpHost } = this.config;
      this.server = this.app.listen(httpPort, httpHost, resolve);
      Cron.initCronJobs();
      // CronExpireCampaigns.initCronJobs();
    });
  }


  /*
  * Returns a promise which stops the server.
  */
  async close() {
    if (!this.server) return this;

    await new Promise((resolve) => {
      this.server.close(resolve);
      this.server = null;
      this.app = null;
    });
  }

}
