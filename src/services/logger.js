const winston = require('winston');
require('winston-mongodb');

const options = {
    db: `${process.env.MONGO_URL}`,
    collection: 'logs',
}

const logger = winston.createLogger({
    transports: [new winston.transports.Console(),
    new winston.transports.MongoDB(options)]
});

module.exports = {
    logger
}