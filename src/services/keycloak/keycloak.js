const session = require('express-session');
const Keycloak = require('keycloak-connect');
const keycloakConf = require('./keycloak-conf.json')

const keycloak = new Keycloak({}, keycloakConf);

const sessionConfig = () => session({
    secret: 'thisShouldBeLongAndSecret',
    resave: false,
    saveUninitialized: true
});

module.exports = {
    keycloak,
    sessionConfig
}