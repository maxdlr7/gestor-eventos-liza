const express = require('express')
const router = express.Router()
const UserController = require('../controllers/user');
const { keycloak } = require('../services/keycloak/keycloak');

/*
* User endpoints
*/

router.get('/', keycloak.protect(), UserController.all);


module.exports = router;