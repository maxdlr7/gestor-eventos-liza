const express = require('express')
const router = express.Router()
const EventController = require('../controllers/event');
const ConditionsController = require('../controllers/conditions');
//events
const EvtSendMailController = require('../controllers/evtSendMail');
const { keycloak } = require('../services/keycloak/keycloak');

/*
* Events endpoints
*/
//====== mantenimientos endpoints ========
router.get('/', EventController.SyncCalls);

module.exports = router;