const request = require('request');
const { realm, server } = require('../services/keycloak/keycloak-conf.json');

/**Solicita un access_token al master realm para ejecutar tareas como crear, actualizar, leer o eliminar un usuario.
*
*/
const generateToken = () => {
    return new Promise(function (resolve, reject) {
        request.post({
            url: `${server}/realms/master/protocol/openid-connect/token`,
            form: {
                'username': `${process.env.KEYCLOAK_USER}`,
                'password': `${process.env.KEYCLOAK_PASSWORD}`,
                'grant_type': 'password',
                'client_id': 'admin-cli'
            }
        }, function (err, response, body) {
            if (err) {
                reject(new Error('Error al generar el token'))
            }
            resolve(JSON.parse(response.body));
        });
    });
}

/**Crea un usuario en Keycloak.
*
* @param {String} token Token generado por Keycloak usando la función generateToken().
* @param {Object} user Un objeto que contiene los campos necesarios para crear un usuario. Ref: http://www.keycloak.org/docs-api/4.0/rest-api/index.html#_userrepresentation
*/
const createUser = (token, user) => {
    return new Promise(function (resolve, reject) {
        request.post({
            url: `${server}/admin/realms/${realm}/users`,
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(user)
        }, function (err, response, body) {
            if (err) {
                reject(new Error('Error al generar el crear el usuario'))
            }
            resolve({ success: true })
        });
    });
}

/** Actualiza un usuario en Keycloak.
 *
 * @param {String} token Token generado por Keycloak usando la función generateToken().
 * @param {String} id id/sub del usuario en Keycloak.
 * @param {Object} user Un objeto que contiene los campos del usuario que se quieren actualizar. Ref: http://www.keycloak.org/docs-api/4.0/rest-api/index.html#_userrepresentation
 */
const udpateUser = (token, id, user) => {
    return new Promise(function (resolve, reject) {
        request.put({
            url: `${server}/admin/realms/${realm}/users/${id}`,
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(user)
        }, function (err, response, body) {
            if (err) {
                reject(new Error('Error al actualizar el usuario'))
            }
            resolve({ success: true })
        });
    });
}

/**Obtiene un usuario en Keycloak basado en un campo y retorna sus atributos.
*
* @param {String} token Token generado por Keycloak usando la función generateToken().
* @param {String} key puede ser email, id/sub, firstName, lastName, username. Ref: http://www.keycloak.org/docs-api/4.0/rest-api/index.html#_users_resource
* @param {String} value puede ser el valor para email, id/sub, firstName, lastName, username.
*/
const getUser = (token, key, value) => {
    return new Promise(function (resolve, reject) {
        request.get({
            url: `${server}/admin/realms/${realm}/users?${key}=${value}`,
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: 'application/json'
            }
        }, function (err, response, body) {
            if (err) {
                reject(new Error('Error al obtener el usuario'))
            }
            resolve(JSON.parse(response.body))
        });
    });
}

/** Obtiene todos los usuarios del realm actual. Ver: src/services/keycloak/keycloak-conf.json
 *
 * @param {String} token Token generado por Keycloak usando la función generateToken().
 */
const getAllUsers = (token) => {
    return new Promise(function (resolve, reject) {
        request.get({
            url: `${server}/admin/realms/${realm}/users`,
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: 'application/json'
            }
        }, function (err, response, body) {
            if (err) {
                reject(new Error('Error al obtener los usuarios'))
            }
            resolve(JSON.parse(response.body))
        });
    });
}

/** Actualiza un usuario en Keycloak.
 *
 * @param {String} token Token generado por Keycloak usando la función generateToken().
 * @param {String} id id/sub del usuario en Keycloak.
 */
const deleteUser = (token, id) => {
    return new Promise(function (resolve, reject) {
        request.delete({
            url: `${server}/admin/realms/${realm}/users/${id}`,
            headers: {
                Authorization: `Bearer ${token}`,
                Accept: 'application/json',
                'Content-Type': 'application/json'
            }
        }, function (err, response, body) {
            if (err) {
                reject(new Error('Error al eliminar el usuario'))
            }
            resolve({ success: true })
        });
    });
}

module.exports = {
    generateToken,
    createUser,
    udpateUser,
    getUser,
    deleteUser,
    getAllUsers
}
