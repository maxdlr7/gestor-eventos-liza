
const Mongoose = require('mongoose');
const moment = require('moment');
const axios = require('axios');
const Call = require('../models/calls');
const CallManagement = require('../models/callManagements');
const Interaction = require('../models/interaction');
require('dotenv').config()
//var remoteConnection = Mongoose.createConnection('mongodb://@localhost:27017/DB-ORCHESTATOR');
var remoteConnection = Mongoose.createConnection(process.env.MONGO_URL_REMOTE);

// stored in 'testB' database
var RemoteCall = remoteConnection.model('calls', Call.calls);

// stored in 'testB' database
var RemoteCallManagement = remoteConnection.model('callmanagements', CallManagement.CallManagement);

// stored in 'testB' database
var RemoteInteraction = remoteConnection.model('interactions', Interaction.interactionSchema);

const SyncCollection = (collectionName) => {
    //We then get the total results from conversations
    try {
        const months = process.env.MONTHS;

        var startDate = moment()
            .subtract(months, 'months').toDate();

        var endDate = moment().toDate();//hora actual

        let collection = null;

        switch (collectionName) {
            case "calls":
                collection = Call;
                break;
            case "callmanagements":
                collection = CallManagement;
                break;
            case "interactions":
                collection = Interaction;
                break;

            default:
                break;
        }
        if (!collection) {
            console.log("No se identifico una collection valida");
            return;
        }

        collection.model.find({ sync: { $ne: true }, dateAdded: { "$gte": startDate, "$lt": endDate } }).then((calls) => {
            console.log("Documentos totales", calls);
            for (let index = 0; index < calls.length; index++) {
                const document = calls[index];
                if (index == 0) {
                    console.log("Documento", JSON.stringify(document, null, 2));
                }
                updateRemoteCall(document, collectionName);
            }
        }).catch((error) => {
            console.error("SyncCollection error: ", error);
        });


    } catch (error) {
        console.log(error);
    }
}


const updateRemoteCall = (document, collectionName) => {
    try {
        let collection = null;
        let remoteCollection = null;

        switch (collectionName) {
            case "calls":
                collection = Call;
                remoteCollection = RemoteCall;
                break;
            case "callmanagements":
                collection = CallManagement;
                remoteCollection = RemoteCallManagement;
                break;
            case "interactions":
                collection = Interaction;
                remoteCollection = RemoteInteraction;
                break;

            default:
                break;
        }
        if (!collection) {
            console.log("No se identifico una collection valida");
            return;
        }

        //insertar la llamada en la base de datos remoto
        remoteCollection.findOneAndUpdate({ "_id": document._id }, document, { upsert: true }).then((result) => {
            //si todo salio bien, actualizar el documento actual y agregar el parametro sync en true
            collection.model.findOneAndUpdate({ "_id": document._id },
                { "sync": true }, {}, (error, document) => {
                    if (error) {
                        console.error("findOneAndUpdate local db error: ", error);
                    } else {
                        console.log("findOneAndUpdate local db ok");
                    }
                });
        }).catch((error) => {
            console.error("updateRemoteCall error: ", error);
        });
    } catch (error) {
        console.log(error);
    }
}

module.exports = { SyncCollection }